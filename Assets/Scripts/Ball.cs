﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	[SerializeField] private AudioClip[] _audio;
    public float speed = 30;

	private AudioSource _audioSource;

	[SerializeField] private MoveRacket _rackets;
	// Use this for initialization



    float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) {
        return (ballPos.y - racketHeight) / racketHeight;
    }

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
		_audioSource = this.gameObject.GetComponent<AudioSource> ();
		_rackets = GameObject.Find ("RacketLeft").GetComponent<MoveRacket>();
	}

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.name == "RacketLeft") {
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
			_audioSource.clip = _audio [0];
			_audioSource.Play ();
            Vector2 dir = new Vector2(1, y).normalized;

            GetComponent<Rigidbody2D>().velocity = dir * speed;
        }

        if (col.gameObject.name == "RacketRight") {
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
			_audioSource.clip = _audio [0];
			_audioSource.Play ();
            Vector2 dir = new Vector2(-1, y).normalized;

            GetComponent<Rigidbody2D>().velocity = dir * speed;
        }

		if (col.gameObject.name == "WallLeft") {
			_audioSource.clip = _audio [1];
			_audioSource.Play ();
			_rackets._enemyScore++;
			this.transform.position = new Vector2 (0, 0);
		}

		if (col.gameObject.name == "WallRight") {
			_audioSource.clip = _audio [1];
			_audioSource.Play ();
			_rackets._playerScore++;
			this.transform.position = new Vector2 (0, 0);
		}
    }
	
}
