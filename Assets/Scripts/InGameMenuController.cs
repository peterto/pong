﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class InGameMenuController : MonoBehaviour {

	[SerializeField] Transform _inGameMenu;
	[SerializeField] GameObject _ball;
	[SerializeField] GameObject _player;
	[SerializeField] GameObject _enemy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (Time.timeScale == 0) {
				if (_inGameMenu.gameObject.activeInHierarchy) {
					Time.timeScale = 1;
					_inGameMenu.gameObject.SetActive (false);
				}
			} else if (Time.timeScale == 1) {
				Time.timeScale = 0;
				_inGameMenu.gameObject.SetActive (true);
			}

		}
	
	}

	public void ExitToMainMenu() {
		SceneManager.LoadScene (0);
	}
		
}
