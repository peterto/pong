﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	public void StartGame() {
		Invoke ("StartGameSlow", 0.2f);
	}

	public void ExitGame() {
		Application.Quit ();
	}

	void StartGameSlow() {
		SceneManager.LoadScene (1);
	}
}
