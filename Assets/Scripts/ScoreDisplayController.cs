﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreDisplayController : MonoBehaviour {

	[SerializeField] private Text [] _scoreText;
	[SerializeField] private MoveRacket _rackets;
	// Use this for initialization
	void Start () {
		_rackets = GameObject.Find ("RacketLeft").GetComponent<MoveRacket>();
	
	}
	
	// Update is called once per frame
	void Update () {
		_scoreText [0].text = _rackets._playerScore.ToString ();
		_scoreText [1].text = _rackets._enemyScore.ToString ();
	}
}
